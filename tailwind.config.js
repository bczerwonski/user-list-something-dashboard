module.exports = {
  purge: {
    content: ['./src/**/*.js'],
    options: {
      safelist: ['bg-red-500', 'bg-green-500', 'bg-yellow-500', 'bg-red-700', 'bg-green-700', 'bg-yellow-700', 'bg-blue-500', 'bg-blue-700'],
    },
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
