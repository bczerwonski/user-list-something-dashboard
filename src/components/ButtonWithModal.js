import React, { useState } from 'react';
import Button from './Button';
import Modal from './Modal';

export default function ButtonWithModal({ children, color, title, Render, onSubmit = () => { }, ...props }) {
    const [visable, setVisable] = useState(false);
    const modalOnSubmit = (data) => { setVisable(false); onSubmit(data); };

    return <>
        <Button color={color} onClick={() => setVisable(true)}>{children}</Button>
        {visable &&
            <Modal onClose={() => setVisable(false)} title={title} >{
                <Render onSubmit={(data) => modalOnSubmit(data)} {...props} />
            }</Modal>}
    </>
}