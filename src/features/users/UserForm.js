import React from 'react';
import { Formik, Form } from 'formik';
import Button from '../../components/Button';
import Field from "../../components/form/Field"

export default function UserForm({ initialValues = { name: '', username: '', address: { city: '' }, city: '' }, onSubmit }) {
    return <Formik
        initialValues={initialValues}
        validate={values => {
            const errors = {};
            if (!values.email) {
                errors.email = 'Required';
            } else if (
                !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
            ) {
                errors.email = 'Invalid email address';
            }
            if (!values.name)
                errors.name = "Rquired"
            return errors;
        }}
        onSubmit={onSubmit}
    >
        {({ isSubmitting }) => (
            <Form className="flex flex-col w-full space-y-2">
                <Field name="name" />
                <Field name="username" />
                <Field type="email" name="email" />
                <Field name="address.city" />
                <Button type="submit" disabled={isSubmitting}>
                    Submit
                </Button>
            </Form>
        )}
    </Formik>
}