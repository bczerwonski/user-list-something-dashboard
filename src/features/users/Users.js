import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { selectUsersByKey, getUsers, postUser, putUser, } from './usersSlice.js';
import ButtonWithModal from '../../components/ButtonWithModal';
import UserForm from './UserForm.js';
import DeleteModal from './DeleteModal';

export function Users() {

    const [order, setOrder] = useState(true);
    const users = useSelector(selectUsersByKey("username", order));

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getUsers())
    }, [dispatch])

    return <div className="flex flex-col">
        <div className="-my-2 overflow-x-auto">
            <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg  mt-8">
                    <table className="min-w-full divide-y divide-gray-200">
                        <thead className="bg-gray-50">
                            <tr>
                                <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">

                                    id
                                </th>
                                <th onClick={() => setOrder((prevState) => !prevState)} scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider cursor-pointer">
                                    username {order ? "🔽" : "🔼"}
                                </th>
                                <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">

                                    <div className="ml-4">
                                        <div>
                                            name
                                        </div>
                                        <div className="text-gray-400">
                                            email
                                        </div>
                                    </div>
                                </th>
                                <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    city
                                </th>
                                <th scope="col" className="px-6 py-3 flex content-center justify-end">
                                    <ButtonWithModal
                                        color="green"
                                        title="Add new user"
                                        Render={UserForm}
                                        onSubmit={(data) => { dispatch(postUser(data)) }}>
                                        Add new
                                    </ButtonWithModal>
                                </th>
                            </tr>
                        </thead>

                        <tbody className="bg-white divide-y divide-gray-200">
                            {users.length === 0 && <tr  ><td className="text-sm font-medium text-gray-900">No users</td></tr>}
                            {users.map((user) => {
                                return <tr key={user.id} className="hover:bg-gray-50">
                                    <td className="px-6 py-4 whitespace-nowrap">
                                        <div className="ml-4">
                                            <div className="text-sm font-medium text-gray-900">
                                                {user.id}
                                            </div>
                                        </div>
                                    </td>
                                    <td className="px-6 py-4 whitespace-nowrap">
                                        <div className="ml-4">
                                            <div className="text-sm font-medium text-gray-900">
                                                👤 {user.username}
                                            </div>
                                        </div>
                                    </td>
                                    <td className="px-6 py-4 whitespace-nowrap">
                                        <div className="ml-4">
                                            <div className="text-sm font-medium text-gray-900">
                                                {user.name}
                                            </div>
                                            <div className="text-sm text-gray-500">
                                                <a href={`mailto: ${user.email}`}>📧{user.email}</a>

                                            </div>
                                        </div>
                                    </td>
                                    <td className="px-6 py-4 whitespace-nowrap">
                                        <div className="ml-4">
                                            <div className="text-sm font-medium text-gray-900">
                                                🌇 {user.address.city}
                                            </div>
                                        </div>
                                    </td>
                                    <td className="px-6 py-4 text-right space-x-1 space-y-1">
                                        <ButtonWithModal
                                            color="yellow"
                                            title={`Edit${user.name}`}
                                            Render={UserForm}
                                            initialValues={user}
                                            onSubmit={(data) => dispatch(putUser(data))}>
                                            Edit
                                    </ButtonWithModal>
                                        <DeleteModal user={user} />
                                    </td>
                                </tr>
                            })}
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div >
}