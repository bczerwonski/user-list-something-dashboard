import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

var i = 0;

export const getUsers = createAsyncThunk("users/getUsers", async () => {
    const res = await fetch("https://my-json-server.typicode.com/karolkproexe/jsonplaceholderdb/data")
    return await res.json()
})

export const postUser = createAsyncThunk("users/postUser", async (data) => {
    const newUser = { id: ++i, ...data }
    // const res =
    await fetch("https://my-json-server.typicode.com/karolkproexe/jsonplaceholderdb/data", {
        method: 'POST',
        newUser
    })
    return newUser
})

export const putUser = createAsyncThunk("users/putUsers", async (data) => {
    // const res =
    await fetch(`https://my-json-server.typicode.com/karolkproexe/jsonplaceholderdb/data/${data.id}`, {
        method: 'PUT',
        data
    })
    return data
})

export const deleteUser = createAsyncThunk("users/deleteUser", async (id) => {
    // const res =
    await fetch(`https://my-json-server.typicode.com/karolkproexe/jsonplaceholderdb/data/${id}`, {
        method: 'DELETE'
    })
    return id
})

export const usersSlice = createSlice({
    name: "users",
    initialState: { users: [] },
    extraReducers: {
        [getUsers.fulfilled]: (state, { payload }) => {
            i = payload.length
            state.users = payload
        },
        [postUser.fulfilled]: (state, { payload }) => {
            state.users.push(payload)
        },
        [putUser.fulfilled]: ({ users }, { payload }) => {
            const id = users.findIndex(({ id }) => id === payload.id)
            users[id] = payload
        },
        [deleteUser.fulfilled]: ({ users }, { payload }) => {
            const idInArray = users.findIndex((value) => value.id === payload)
            users.splice(idInArray, 1)
        }
    }
})

export const { increment } = usersSlice.actions

export const selectUsers = ({ users }) => users.users
export const selectUsersByKey = (key, asc = true) => ({ users }) => {
    let arr = [...users.users];
    arr.sort((a, b) => {
        const [aa, bb] = [a[key], b[key]];
        if (bb > aa)
            return -1;
        if (aa > bb)
            return 1;
        return 0;
    });
    if (!asc)
        arr.reverse()
    return arr;
}

export default usersSlice.reducer