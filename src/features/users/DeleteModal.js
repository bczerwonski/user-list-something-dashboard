import React from 'react';
import { useDispatch } from 'react-redux';
import { deleteUser } from './usersSlice.js';
import ButtonWithModal from '../../components/ButtonWithModal';
import Button from '../../components/Button';

export default function DeleteModal({ user }) {
    return <ButtonWithModal color="red" title={`Delete ${user.username}`} Render={Confirm} user={user}> Delete</ButtonWithModal >
}

function Confirm({ user, onSubmit }) {
    const dispatch = useDispatch();
    return <div>
        <div className="pb-4">Are you sure you want to delete <label className="font-bold">{user.username}</label>?</div>
        <Button color="red" onClick={() => { dispatch(deleteUser(user.id)); onSubmit() }}>Delete</Button>
    </div>
}